# lepindex

A snapshot of the source files used with the import script to get LepIndex into TaxonWorks.

Important historical notes:

* This snapshot is NOT a clone of one of the repositories we used to pass data around.  
* It is a verbatim copy of the files in the SFG staging folder where only data incoming to production TaxonWorks lived.  I.e. it is certain they are the original source.
* These data are for historical purposes and data-checking, you really shouldn't derive something further from them, as that will confuse.


