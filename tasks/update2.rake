require 'fileutils'

### rake tw:project_import:lepindex:lepindex_update2
### rake tw:db:restore backup_directory=/Users/proceps/src/dump/ file=94cc5596e185b333_localhost_2020-05-12_135602UTC.dump
# ./bin/webpack-dev-server



namespace :tw do
  namespace :project_import do
    namespace :lepindex do

      desc 'Lepindex update 2'
      task :lepindex_update2 do |t|

        main_build_loop_lepindex_update2

      end

      def main_build_loop_lepindex_update2
        print "\nStart time: #{Time.now}\n"

        Current.project_id = 5
        Current.user_id = 2
        raise 'Current.project_id or $user_id not set.'  if Current.project_id.nil? || Current.user_id.nil?

        verbatim_authors_lepindex_update2
        original_combination_lepindex_update2

        print "\n\n !! Success. End time: #{Time.now} \n\n"
      end

      def verbatim_authors_lepindex_update2
        print "\nCreate author roles\n"

        au_list = {
          'Walker, F.' => 46272,
          'Lucas, T.P.' => 141133,
          'Linnaeus' => 38532,
          'Fabricius' => 33438,
        }
        i = 0

        au_list.each do |key, value|
          TaxonName.where(project_id: 5, verbatim_author: key).find_each do |t|
            t.update_column(:verbatim_author, nil)
            TaxonNameAuthor.create(person_id: value, role_object: t)
            i += 1
            print "\rRoles created: #{i}"
          end
        end
      end

      def original_combination_lepindex_update2
        print "\nUpdate original combination\n"
        i = 0
        TaxonName.where(project_id: 5).where("cached_original_combination ILIKE '%SPECIES NOT SPECIFIED%'").each do |t|
          if t.rank_string == 'NomenclaturalRank::Iczn::SpeciesGroup::Subspecies'
            if !t.original_genus.nil? && t.original_species.nil? && t.original_subspecies.nil? && t.original_variety.nil? && t.original_form.nil?
              t.original_species = t
              t.save
              i += 1
              print "\rOriginal combinations updated: #{i}"
            end
          end
        end
      end
    end

  end
end
