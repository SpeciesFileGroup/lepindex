require 'fileutils'

### rake tw:project_import:lepindex:lepindex_update1
### rake tw:db:restore backup_directory=/Users/proceps/src/dump/ file=94cc5596e185b333_localhost_2020-05-12_135602UTC.dump
# ./bin/webpack-dev-server



namespace :tw do
  namespace :project_import do
    namespace :lepindex do

      desc 'Lepindex update 1'
      task :lepindex_update1 do |t|

        main_build_loop_lepindex_update1

      end

      def main_build_loop_lepindex_update1
        print "\nStart time: #{Time.now}\n"

        Current.project_id = 5
        Current.user_id = 2
        raise 'Current.project_id or $user_id not set.'  if Current.project_id.nil? || Current.user_id.nil?

        run_lepindex_update1

        print "\n\n !! Success. End time: #{Time.now} \n\n"
      end

      def run_lepindex_update1
        kw = Keyword.find_or_create_by(name: 'incertae_sedis', definition: 'Species originally linked to the genera named Species', project_id: Current.project_id).id

        taxa = Protonym.where(name: 'Species', rank_class: 'NomenclaturalRank::Iczn::GenusGroup::Genus', project_id: Current.project_id)
        taxa.each do |genus|
          genus.descendants.each do |species|
            species.parent_id = genus.parent_id
            species.save
            if species.id == species.cached_valid_taxon_name_id
              tr = TaxonNameRelationship::Iczn::Validating::UncertainPlacement.create!(subject_taxon_name_id: species.id, object_taxon_name_id: species.parent_id)
              tg = Tag.create!(tag_object: species, keyword_id: kw)
            end
          end
          genus.otus.first.destroy unless genus.otus.first.nil?
          genus.destroy
        end
      end

    end

  end
end
